# weather

This is a sample web app as requested for altran

> **Note:** Weather API key is not providen here

## quick start

Clone the repo

```
npm i
npm start
```

Open browser on localhost:3000

**Requirements:** Node > v6 npm > 3 is required

## api

api.openweathermap.org/data/2.5/weather

## requested cities

```json
  {
    "id": 6357346,
    "name": "Santiago de Compostela",
    "country": "ES",
    "coord": {
      "lon": -8.54736,
      "lat": 42.880241
    }
  },
  {
    "id": 3435907,
    "name": "Provincia de Buenos Aires",
    "country": "AR",
    "coord": {
      "lon": -60,
      "lat": -36
    }
  },
  {
    "id": 3936456,
    "name": "Lima",
    "country": "PE",
    "coord": {
      "lon": -77.028236,
      "lat": -12.04318
    }
  },

  {
    "id": 3448439,
    "name": "Sao Paulo",
    "country": "BR",
    "coord": {
      "lon": -46.636108,
      "lat": -23.547501
    }
  },
```

## License

Unlicensed