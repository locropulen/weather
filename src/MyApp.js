// @flow

import React, { PureComponent } from 'react';

import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';

import { loadMe, load } from './actions';

import { InputGroup, InputGroupAddon, Input, InputGroupButton, Button, Breadcrumb, BreadcrumbItem, Card, CardText, CardBlock,  CardTitle, CardSubtitle, CardImg, Badge, Alert } from 'reactstrap';

const Layout = (props: any) =>
  <div style={styles.layout}>
    <Breadcrumb>
      <BreadcrumbItem active>Altran test react redux by agrcrobles</BreadcrumbItem>
    </Breadcrumb>
    {props.error &&
      <Alert color="danger">
        <strong>Oh snap!</strong> There was an error while getting the weather
      </Alert>
    }
    {props.children}
  </div>;

class MyApp extends PureComponent<*, *, *> {
  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      key: ''
    }
    this.load = this.load.bind(this);
    // setInterval(this.load, 4000);
    this.onDismiss = this.onDismiss.bind(this);
  }
  onDismiss: Function;
  onDismiss() {
    this.setState({ visible: false });
  }
  load: Function;
  load() {
    this.props.load(this.state.key);
  }
  componentDidMount() {
    // this.load();
  }
  render() {
    if (Object.keys(this.props.dates).length === 0 || this.props.error) {
      return (
        <Layout {...this.props} >
          <InputGroup style={styles.InputGroup}>
            <InputGroupAddon>http://openweathermap.org</InputGroupAddon>
            <Input placeholder="Key" onChange={evt => this.setState({key:  evt.target.value})}/>
            <InputGroupButton onClick={this.load}>Load me!</InputGroupButton>
          </InputGroup>
          
        </Layout>
      );
    }
    return (
      <Layout>
        {Object.keys(this.props.dates).map((key: string) => {
          return Object.keys(this.props.dates[key].weather).map((subkey: string) => {
            const city = this.props.dates[key].weather[subkey];

            const imageURL = `http://openweathermap.org/img/w/${this.props.dates[key].weather[subkey].weather[0].icon}.png`;

            return (
              <div key={subkey} style={styles.cardContainer}>
                <Card>
                  <CardBlock>
                    <CardTitle>{city.name}<CardImg top src={imageURL} alt="Weather" /></CardTitle>
                    <p>Weather: {city.main.temp}<Badge style={styles.badge}>Kelvin</Badge></p>
                    <p>Humidity: {city.main.humidity}<Badge style={styles.badge}>percent</Badge></p>
                    <p>Temp max: {city.main.temp_max}<Badge style={styles.badge}>Kelvin</Badge></p>
                    <p>Temp min: {city.main.temp_min}<Badge style={styles.badge}>Kelvin</Badge></p>
                  </CardBlock>
                </Card>
              </div>
            );
          })
        })}
        <Alert color="info" isOpen={this.state.visible} toggle={this.onDismiss}>
          React + Redux app that consunes API from http://openweathermap.org/api
        </Alert>
      </Layout>
    );
  }
}

const styles = {
  layout: {
    display: 'flex',
    flex: 1,
    padding: 10,
    flexDirection: 'column'
  },
  cardContainer: {
    padding: "10px 0"
  },
  badge: {
    margin: "0 5px"
  },
  InputGroup: {
    padding: "10px 0"
  }
}
const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			load
		},
		dispatch
	);
export default connect(state => ({
  error: state.weather.error,
  dates: state.weather.byDate
}), mapDispatchToProps)(MyApp);
