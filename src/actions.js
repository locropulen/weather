// @flow

type Dispatch = () => void;

export type TemplatesAction = {
	type: 'LOAD_WEATHER'
} | {
  type: 'LOAD_WEATHER_ERROR'
};

export const load = (key: string) => {  
  return async (dispatch: Dispatch, getState: any) => {
  	try {
      // stgo de compostela
      const responseSgo = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=6357346&APPID=${key}`);
        // bs as
      const responseBsAs = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=3435907&APPID=${key}`);
        //lima
      const responseLima = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=3936456&APPID=${key}`);
        // sao paulo
      const responseSaoPaulo = await fetch(`http://api.openweathermap.org/data/2.5/weather?id=3448439&APPID=${key}`);
      
      const Sgo = await responseSgo.json();
      const BsAs = await responseBsAs.json();
      const Lima = await responseLima.json();
      const SaoPaulo = await responseSaoPaulo.json();
  
      if (Sgo.cod !== 401 && BsAs.cod !== 401 || Lima.cod !== 401 || SaoPaulo.cod !== 401) {
        dispatch({
          type: 'LOAD_WEATHER',
          date: new Date(),
          index: Object.keys(getState().weather.byDate).length + 1,
          json: {
            Sgo,
            BsAs,
            Lima,
            SaoPaulo
          }
        });

      }

  	} catch (ex) {
      dispatch({
        type: 'LOAD_WEATHER_ERROR',
      });
  	}
  };
};