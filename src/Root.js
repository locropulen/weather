// @flow

import React from 'react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import MyApp from './MyApp';

import { applyMiddleware, createStore, combineReducers } from 'redux';

import weatherReducer from './weatherReducer';

const rootReducer = combineReducers({
  weather: weatherReducer,
});

const store = createStore(
  rootReducer,
  applyMiddleware(thunk),
);

export default function Root() {
  return (
    <Provider store={store}>
      <MyApp />
    </Provider>
  );
}
