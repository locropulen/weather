// @flow

const weatherReducer = (state = {
  byDate: {},
  error: false
}, action) => {
  switch (action.type) {
    case 'LOAD_WEATHER_ERROR':
      return {
        ...state,
        error: true
      }
    case 'LOAD_WEATHER':
      console.log("heyy")
      return {
        byDate: {
          [action.index]: {
            weather: action.json,
            date: action.date
          },
          ...state.byDate,
        },
        error: false
      };
  }
  return state;
};

export default weatherReducer;
