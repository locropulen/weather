/**
 * Sample App
 * @flow
 */

import 'bootstrap/dist/css/bootstrap.css';

import React from 'react';

import Root from './src/Root';

import ReactDOM from 'react-dom';

ReactDOM.render(
  <Root />,
  document.getElementById('root'),
);
